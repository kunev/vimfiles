setlocal ts=2
setlocal shiftwidth=2
setlocal softtabstop=2
setlocal expandtab

let g:gutentags_ctags_executable_ruby = 'ripper-tags'
let g:gutentags_ctags_extra_args = ['--ignore-unsupported-options', '--recursive']

let g:rubycomplete_buffer_loading = 1
let g:rubycomplete_classes_in_global = 1
let g:rubycomplete_use_bundler = 1

fun RunSpecVimux()
  let filename = expand('%:p')
  if filename =~ '_spec.rb$'
    let filename = filename
  else
    let filename = substitute(substitute(filename, '/app/', '/spec/', ''), '.rb$', '_spec.rb', '')
  endif
  let l:command = 'bin/rspec -f d ' . filename
  call VimuxRunCommand(l:command)
endfunc

fun RunSpecCaseVimux()
  let filename = expand('%')
  let l:command = 'bin/rspec -f d ' . filename . ':' . line('.')
  call VimuxRunCommand(l:command)
endfun

nnoremap <buffer> <leader>sr :call RunSpecVimux()<cr>
nnoremap <buffer> <leader>cr :call RunSpecCaseVimux()<cr>
nnoremap <buffer> <leader>bp Obinding.pry<esc>

call ale#Set('ruby_rubocop_executable', 'bundle')
