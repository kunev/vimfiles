let g:terraform_fmt_on_save=1
let g:terraform_fold_sections=0
let g:terraform_remap_spacebar=0
let g:terraform_commentstring='#%s'
