setlocal ts=4
setlocal expandtab
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal tabstop=8
setlocal list
setlocal colorcolumn=80

setlocal listchars=tab:╞═,trail:•,extends:❯,precedes:❮

filetype indent on

let python_highlight_all = 1

"Insert vimpdb breakpoint
nnoremap <buffer> <leader>bp oimport vimpdb; vimpdb.set_trace()
let g:ale_python_pyls_executable = "pylsp"

BracelessEnable +highlight +fold

let b:ale_python_auto_pipenv = 1
let b:ale_python_isort_auto_pipenv = 1
let b:ale_python_mypy_auto_pipenv = 1
