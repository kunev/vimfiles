let s:path = expand('<sfile>:p:h')
call plug#begin(s:path . '/plugged')

" libs
"

" filetypes
" Plug 'sheerun/vim-polyglot'
Plug 'chrisbra/csv.vim', {'for': 'csv'}
Plug 'othree/html5.vim', {'for': 'html'}
Plug 'pangloss/vim-javascript', {'for': 'javascript'}
Plug 'kchmck/vim-coffee-script'
Plug 'juvenn/mustache.vim', {'for': 'mustache'}
Plug 'zah/nim.vim', {'for': 'nim'}
Plug 'vim-perl/vim-perl', {'for': 'perl'}
Plug 'derekwyatt/vim-scala', {'for': 'scala'}
Plug 'kovisoft/paredit', {'for': ['clojure', 'scheme', 'lisp']}
Plug 'kovisoft/slimv', {'for': ['clojure', 'scheme', 'lisp']}
Plug 'Matt-Deacalion/vim-systemd-syntax', {'for': 'systemd'}
Plug 'fatih/vim-go', {'for': 'go', 'do': ':GoUpdateBinaries'}
Plug 'sebdah/vim-delve', {'for': 'go'}
Plug 'slim-template/vim-slim'
Plug 'tmux-plugins/vim-tmux'
Plug 'cespare/vim-toml'
Plug 'solarnz/thrift.vim'
Plug 'keith/swift.vim'
Plug 'digitaltoad/vim-pug'
Plug 'buoto/gotests-vim', {'for': 'go'}
Plug 'supercollider/scvim'
Plug 'hashivim/vim-terraform', {'for': 'terraform'}
Plug 'thecodesmith/vim-groovy'
Plug 'yuezk/vim-js'
Plug 'MaxMEllon/vim-jsx-pretty'
Plug 'jparise/vim-graphql'
Plug 'Fymyte/rasi.vim'
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-bundler'
Plug 'tpope/vim-rails'
Plug 'jxnblk/vim-mdx-js'
Plug 'jamespeapen/swayconfig.vim'
Plug 'briancollins/vim-jst'
Plug 'lifepillar/pgsql.vim'
Plug 'https://gitlab.com/HiPhish/jinja.vim'
Plug 'rust-lang/rust.vim'
Plug 'bfrg/vim-jqplay'
Plug 'leafOfTree/vim-vue-plugin'

" niceties
Plug 'AndrewRadev/bufferize.vim'
Plug 'AndrewRadev/deleft.vim'
Plug 'AndrewRadev/dsf.vim'
Plug 'AndrewRadev/sideways.vim'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'AndrewRadev/switch.vim'
Plug 'AndrewRadev/writable_search.vim'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'bronson/vim-visual-star-search'
Plug 'brooth/far.vim' " Find And Replace
Plug 'godlygeek/tabular' " align around characters
Plug 'itchyny/lightline.vim'
Plug 'jiangmiao/auto-pairs' " input pairs of braces/brackets/quotes/etc.
Plug 'lyokha/vim-xkbswitch'
Plug 'mg979/vim-visual-multi'
Plug 'michaeljsmith/vim-indent-object'
Plug 'preservim/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-endwise'
Plug 'suy/vim-context-commentstring'
Plug 'tpope/vim-surround'
Plug 'tweekmonster/braceless.vim' " text object for file types without braces
Plug 'vim-scripts/ZoomWin'
Plug 'vim-utils/vim-husk'  " fancy command mode
Plug 'wincent/loupe' " search niciteis
Plug 'romainl/vim-qf' " quickfix stuff
Plug 'alvan/vim-closetag'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-unimpaired'
Plug 'jasonccox/vim-wayland-clipboard'
Plug 'tkhren/vim-fake'

" spelling and grammar
Plug 'szw/vim-dict' " autdownload spell files
Plug 'rhysd/vim-grammarous'
Plug 'shinglyu/vim-codespell'

" focus porn
Plug 'junegunn/goyo.vim'

" completers/snippets
Plug 'mattn/emmet-vim', {'for': ['html', 'xml', 'htmldjango', 'eruby', 'javascriptreact', 'typescriptreact', 'markdown.mdx']}

" external tool integrations
Plug 'airblade/vim-gitgutter'
Plug 'benmills/vimux'
Plug 'jmcantrell/vim-virtualenv', {'for': 'python'}
if has('nvim') == 1
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'
    Plug 'duane9/nvim-rg'
else
    Plug 'coreyja/fzf.devicon.vim'
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
endif
Plug 'rhysd/conflict-marker.vim'
Plug 'rhysd/git-messenger.vim'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
Plug 'PieterjanMontens/vim-pipenv', {'for': 'python'}

" motion/object
Plug 'kana/vim-smartword'

" tags
Plug 'ludovicchabant/vim-gutentags'
Plug 'majutsushi/tagbar'

" errors
Plug 'dense-analysis/ale'
Plug 'itspriddle/vim-shellcheck'


" eye candy
Plug 'Yggdroot/indentLine'
Plug 'ap/vim-css-color', { 'for': ['css', 'sass'] }
Plug 'chriskempson/base16-vim'
Plug 'guns/xterm-color-table.vim'
Plug 'jlanzarotta/colorSchemeExplorer'
Plug 'ryanoasis/vim-devicons'
call plug#end()
