unlet b:current_syntax
syn include @SQL syntax/sql.vim
syn region sqlHeredoc start=/\v\<\<[-~]SQL/ end=/\vSQL/ keepend contains=@SQL
let b:current_syntax = "ruby"

unlet b:current_syntax
syn include @GRAPHQL syntax/graphql.vim
syn region graphqlHeredoc start=/\v\<\<[-~]GRAPHQL/ end=/\GRAPHQL/ keepend contains=@GRAPHQL
let b:current_syntax = "ruby"

unlet b:current_syntax
syn include @ELASTIC syntax/json.vim
syn region esQueryHeredoc start=/\v\<\<[-~]ELASTIC/ end=/\ELASTIC/ keepend contains=@ELASTIC
let b:current_syntax = "ruby"

unlet b:current_syntax
syn include @ELASTIC syntax/json.vim
syn region esQueryHeredoc start=/\v\<\<[-~]JSON/ end=/\JSON/ keepend contains=@JSON
let b:current_syntax = "ruby"
