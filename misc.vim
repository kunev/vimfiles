function! StartUpNERDTree()
	if &ft !=# 'vim' && &ft !=# 'diff'
		exec 'NERDTree'
	endif
endfunction

function! BufNames()
    let bufcount = buffer_number('$')
    let active_buffer_number = buffer_number('%')
    let curbufnr = 1
    let buffs = ''
    while curbufnr <= bufcount
        let bufname = buffer_name(curbufnr)
        let bufname = fnamemodify(bufname, ':.')
        if(curbufnr == active_buffer_number)
            let bufname = '✱' . bufname
        else
            let bufname = ' ' . bufname
        endif
        if(buflisted(curbufnr))
            let buffs = (buffs . bufname . ' ')
        endif
        let curbufnr = curbufnr + 1
    endwhile
    return buffs
endfunction

function! DoRemote(arg)
  UpdateRemotePlugins
endfunction


function! StartVisualTask() range
    " Get the line and column of the visual selection marks
    let [lnum1, col1] = getpos("'<")[1:2]
    let [lnum2, col2] = getpos("'>")[1:2]

    " Get all the lines represented by this range
    let lines = getline(lnum1, lnum2)

    " The last line might need to be cut if the visual selection didn't end on the last column
    let lines[-1] = lines[-1][: col2 - (&selection == 'inclusive' ? 1 : 2)]
    " The first line might need to be trimmed if the visual selection didn't start on the first column
    let lines[0] = lines[0][col1 - 1:]

    " Get the desired text
    let selectedText = join(lines, "\n")

    execute "silent !timew stop"
    execute "silent !timew start \"" . selectedText . " \""
    execute "silent redraw!"
endfunction

function! RemoveCurrentTimewarriorTimer()
   execute "silent !timew && timew delete @1"
   execute "silent redraw!"
endfunction

function! StopCurrentTimewarriorTimer()
   execute "silent !timew stop"
   execute "silent redraw!"
endfunction


" make quickfix navigation nicer
command! Cnext try | cnext | catch | cfirst | catch | endtry
command! Cprev try | cprev | catch | clast | catch | endtry
command! Lnext try | lnext | catch | lfirst | catch | endtry
command! Lprev try | lprev | catch | llast | catch | endtry
