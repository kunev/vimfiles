let mapleader="\<space>"

"Toggle text wrapping with <leader>tw
nnoremap <leader>tw :set nowrap!<cr>

"Toggle NERDTree with <leader>nt
nnoremap <leader>nt :NERDTreeToggle<cr>

"Use <leader>w to save all open buffers
nnoremap <leader>w :w<cr>

"Close current buffer and window
nnoremap <leader>cb :bd \| bd #<cr>

"Switch windows with Tab/Shift Tab in normal mode
nnoremap <tab> <c-w><c-w>
nnoremap <s-tab> <c-w>W

"Move always by visual line
nnoremap j gj
nnoremap k gk

"Toggle taglist with <leader>t
nnoremap <leader>tt :TagbarToggle<cr>

"Stop highlighting the last search
nnoremap <c-h> :nohlsearch<cr>

"Swap normal and relative line numbers
nnoremap <leader>tr :set relativenumber!<cr>

" Toggle line numbers
nnoremap <leader>tn :set numbers!<cr>

"<c-c> is easier to use than <c-[>
inoremap <c-c> <c-[>

" J/K work as usual in nerd tree
let g:NERDTreeMapJumpFirstChild='-'
let g:NERDTreeMapJumpLastChild='-'

nnoremap <silent> <leader>? :execute 'vimgrep /'.@/.'/g %'<CR>:copen<CR>

"Write files as root
cmap w!! w !sudo tee >/dev/null %

"Mappings for fugitive
nnoremap <leader>gb :Git blame<cr>
nnoremap <leader>gd :Gdiff <cr>
nnoremap <leader>dq :diffoff <cr>:q<cr>:e<cr>

"Navigate buffers
nnoremap <leader>j :bnext<cr>
nnoremap <leader>k :bprev<cr>

"Splitjoin
nmap <leader>J :SplitjoinJoin<cr>
nmap <leader>S :SplitjoinSplit<cr>

"Show error window with <leader>e
nnoremap <leader>e :Errors<cr>

"Toggle Gudno with gu
nnoremap <leader>gg :GundoToggle<cr>

"Toggle list
nnoremap <leader>tl :set list!<cr>

nnoremap <leader>m :Multichange<cr>

nnoremap H :SidewaysLeft<cr>
nnoremap L :SidewaysRight<cr>

nnoremap <leader>mr :CtrlPMRUFiles<cr>
nnoremap <leader>/ :CtrlPLine<cr>

nnoremap <leader>a :A<cr>

" fzf mappings
if has('nvim')
    nnoremap <c-p> :Telescope find_files<cr>
    nnoremap <leader>e :Telescope buffers<cr>
    nnoremap <c-t> :Telescope tags<cr>
    " nnoremap <C-g> :Telescope git_commits<cr>
    " nnoremap <leader>* :Rg <c-r><c-w><cr>
    " nnoremap <leader>D :Tags '<c-r><c-w><cr>
    " nnoremap <leader>G :GFiles?<cr>
    " imap <c-x><c-k> <plug>(fzf-complete-word)
    " imap <c-x><c-f> <plug>(fzf-complete-path)
    " imap <c-x><c-j> <plug>(fzf-complete-file-ag)
    " imap <c-x><c-l> <plug>(fzf-complete-line)
else
    nnoremap <c-p> :Files<cr>
    nnoremap <leader>e :Buffers<cr>
    nnoremap <c-t> :Tags<cr>
    nnoremap <C-g> :Commits<cr>
    nnoremap <leader>* :Rg <c-r><c-w><cr>
    nnoremap <leader>D :Tags '<c-r><c-w><cr>
    nnoremap <leader>G :GFiles?<cr>
    imap <c-x><c-k> <plug>(fzf-complete-word)
    imap <c-x><c-f> <plug>(fzf-complete-path)
    imap <c-x><c-j> <plug>(fzf-complete-file-ag)
    imap <c-x><c-l> <plug>(fzf-complete-line)
endif

" mappings for argument text objects
omap aa <Plug>SidewaysArgumentTextobjA
xmap aa <Plug>SidewaysArgumentTextobjA
omap ia <Plug>SidewaysArgumentTextobjI
xmap ia <Plug>SidewaysArgumentTextobjI

" " mappings for smartword
map w  <Plug>(smartword-w)
map b  <Plug>(smartword-b)
map e  <Plug>(smartword-e)
map ge  <Plug>(smartword-ge)
noremap ,w  w
noremap ,b  b
noremap ,e  e
noremap ,ge  ge

vnoremap p "_dP

command Tterm tab term
command Vterm vert term

nnoremap <leader>d <c-]>

nnoremap ; :

" ,x -> :X
" For easier typing of custom commands
nnoremap \      :call <SID>RevSlashMapping(0)<cr>
xnoremap \ :<c-u>call <SID>RevSlashMapping(1)<cr>
function! s:RevSlashMapping(visual)
  echo
  let c = nr2char(getchar())
  if a:visual
    normal! gv
  endif
  call feedkeys(':'.toupper(c))
endfunction

" timewarrior
vnoremap tvt :call StartVisualTask()<cr>
nnoremap dct :call RemoveCurrentTimewarriorTimer()<cr>
nnoremap sct :call StopCurrentTimewarriorTimer()<cr>

nmap <leader>co :copen<cr>

" make NERDTree mappings conform to what fzf and Telescope use
let NERDTreeMapOpenSplit = '<c-x>'
let NERDTreeMapOpenVSplit = '<c-v>'

" ALE
nnoremap <silent> ]e :ALENextWrap<cr>
nnoremap <silent> [e :ALEPreviousWrap<cr>
nnoremap <silent> <c-]> :ALEGoToDefinition<cr>
nnoremap <silent> <c-}> :ALEGoToTypeDefinition -vsplit<cr>
nnoremap <silent> <leader>r :ALEFindReferences -quickfix<cr>
nnoremap <silent> <c-k><c-i> :ALEHover<cr>
nnoremap <silent> <c-q> :ALEDetail<cr>
inoremap <silent> <c-k><c-i> <C-\><C-O>:ALEHover<cr>i
inoremap <silent> <c-o> <C-\><C-O>:ALEComplete<cr>
nnoremap <silent> <leader>i :ALEImport<cr>
nnoremap <silent> <leader>ca :ALECodeAction<cr>
nnoremap <silent> <F2> :ALERename<cr>
inoremap <silent> <F2> :ALERename<cr>
" inoremap <silent><C-P> <C-\><C-O>:call ale#completion#AlwaysGetCompletions()<CR>

" quickfix & locationlist
nnoremap <leader>q <Plug>(qf_qf_toggle)<cr>
nnoremap <leader>l <Plug>(lf_lf_toggle)<cr>

nnoremap <c-f> :Rg<space>
nnoremap <c-s>f :execute "Rg " .. expand('<cword>')<cr>

" easy way to redraw and rehighlight if things get messed up
nnoremap <c-l> :redraw<cr>:syn sync fromstart<cr>

" vimux
nnoremap <leader>vl :VimuxRunLastCommand<cr>
nnoremap <leader>vi :VimuxInspectRunner<cr>


" fix the last spelling mistake and continue at the same insert mode position
" with ctrl-l
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u
