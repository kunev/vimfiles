scriptencoding utf-8
set encoding=utf-8

let s:path = expand('<sfile>:p:h')

execute "source" . s:path . "/misc.vim"
execute "source" . s:path . "/plugs.vim"
execute "source" . s:path . "/settings.vim"
execute "source" . s:path . "/mappings.vim"
execute "source" . s:path . "/commands.vim"

"Close vim if the only window left is NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

if &term == "alacritty"
    let &term = "xterm-256color"
endif

let g:ackhighlight=1
let g:ackprg = 'ag --nogroup --nocolor --column'
let g:airline_powerline_fonts = 1
let g:bufferline_fname_mod = ':.'

if $THEME == 'light'
    set background=light
    color base16-tomorrow
else
    set background=dark
    color base16-tomorrow-night
endif

let $FZF_DEFAULT_COMMAND='fd --type f --strip-cwd-prefix'

set t_Co=256
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
set termguicolors


if has("gui_running")
    set go=
    set guifont=monospace\ 15
else
    highlight Normal ctermbg=NONE guibg=NONE
    highlight NonText ctermbg=NONE guibg=NONE
    au ColorScheme * hi Normal ctermbg=NONE guibg=NONE
endif

let g:vimwiki_list = [{
          \ 'syntax': 'default',
          \ 'path': '$HOME/vimwiki',
          \ 'template_path': s:path . '/vimwiki_template',
          \ 'template_default': 'default',
          \ 'template_ext': '.html'}]

let &t_SI = "\e[6 q"
let &t_EI = "\e[2 q"

" reset the cursor on start (for older versions of vim, usually not required)
augroup myCmds
au!
autocmd VimEnter * silent !echo -ne "\e[2 q"
augroup END
