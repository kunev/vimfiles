filetype plugin on
filetype plugin indent on
syntax enable
syntax sync fromstart

set autoindent
set smartindent
set expandtab
set number
set hlsearch
set incsearch
set modeline
set cursorline
set noautoread
set splitright
set splitbelow

set background=dark
set showtabline=2
set ts=4
set shiftwidth=4
set numberwidth=2
set laststatus=2
set softtabstop=4
set mouse=
set diffopt=filler,vertical,iwhite
set listchars=tab:»═,trail:•,extends:❯,precedes:❮
set fillchars=vert:∥,fold:≣
set signcolumn=yes
set synmaxcol=0

set list
set backup
set backupdir=~/.vim/backups
set backspace=indent,eol,start
set termguicolors
set t_ut=
set noshowmode " lightline shows the mode
set omnifunc=ale#completion#OmniFunc
set foldlevel=20
set wildmenu
set wildmode=full

if !has('nvim')
    set wildoptions=fuzzy
    set ttymouse=xterm2
endif

if exists("g:neovide")
    set guifont=monospace:h12
endif

let s:path = expand('<sfile>:p:h')
let &directory = s:path . '/swap'

"Local vimrc settings
let g:localvimrc_sandbox=0
let g:localvimrc_persistent=1

let g:bl_no_mappings=1

let g:XkbSwitchEnabled = 1
let g:XkbSwitchIMappingsTrData = s:path . '/xkbswitch.tr'
let g:XkbSwitchIMappings = [ 'bg' ]
let g:XkbSwitchLib = '/usr/lib/libxkbswitch.so'

let g:sclangTerm = 'termite -e'

let g:signify_vcs_list = [ 'git', 'hg' ]
let g:signify_sign_add               = '⨭'
let g:signify_sign_change            = '≏'
let g:signify_sign_delete            = '_'
let g:signify_sign_delete_first_line = '‾'

let g:pymode_folding = 0

hi SpecialKey ctermbg=none

let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

let g:rbpt_max = 16
let g:rbpt_loadcmd_toggle = 0
let g:gutentags_cache_dir = s:path . "/tags"

let g:gruvbox_contrast_dark='hard'
let g:argwrap_wrap_closing_brace=0

let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit'}
let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.8 } }

let g:lightline = {
      \ 'colorscheme': 'powerline',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'fugitive', 'gitstatus', 'readonly', 'modified', 'buffers' ] ]
      \ },
      \ 'component': {
      \   'readonly': '%{&filetype=="help"?"":&readonly?"⛔":""}',
      \   'modified': '%{&filetype=="help"?"":&modified?"🔄":&modifiable?"":"✎⃠"}',
      \   'fugitive': '%{exists("*FugitiveHead")?("🫒".FugitiveHead()):""}',
      \   'buffers' : '%{BufNames()}',
      \   'gitstatus': '%{GitStatus()}'
      \ },
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead'
      \ },
      \ 'component_visible_condition': {
      \   'readonly': '(&filetype!="help"&& &readonly)',
      \   'modified': '(&filetype!="help"&&(&modified||!&modifiable))',
      \   'fugitive': '(exists("*FugitiveHead") && ""!=FugitiveHead())'
      \ },
      \ 'mode_map': {
      \  'n' : ' N ',
      \  'i' : ' I ',
      \  'R' : ' R ',
      \  'v' : ' V ',
      \  'V' : 'VL ',
      \  "\<C-v>": 'VB ',
      \  'c' : ' C ',
      \  's' : ' S ',
      \  'S' : 'SL ',
      \  "\<C-s>": 'SB ',
      \  't': ' T ',
      \ },
\ }

let g:indentLine_setColors = 0
let g:indentLine_char = '│'
let g:indentLine_enabled = 0


let g:LoupeVeryMagic = 0

let g:gitgutter_sign_added = ''
let g:gitgutter_sign_modified = '≏'
let g:gitgutter_sign_removed = ''
let g:gitgutter_sign_removed_first_line = '‾‾'
let g:gitgutter_sign_modified_removed = '--'
let g:gitgutter_realtime = 1
let g:gitgutter_eager = 1

function! GitStatus()
    return join(filter(map([g:gitgutter_sign_added,g:gitgutter_sign_modified,g:gitgutter_sign_removed], {i,v -> v.': '.GitGutterGetHunkSummary()[i]}), 'v:val[-1:]'), ' ')
endfunction

let g:notes_directories=['~/Documents/notes']

let g:calendar_google_calendar = 1
let g:calendar_google_task = 1

let g:snipMate = {}
let g:snipMate.snippet_version = 1

set completeopt=menuone,noinsert,noselect,preview

" ALE
let g:ale_floating_preview = 1
let g:ale_floating_window_border = repeat([''], 6)
let js_fixers = ['prettier', 'eslint']
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'javascript': js_fixers,
\   'javascript.jsx': js_fixers,
\   'typescript': js_fixers,
\   'typescriptreact': js_fixers,
\   'vue': js_fixers,
\   'markdown.mdx': ['prettier'],
\   'css': ['prettier'],
\   'json': ['prettier'],
\   'python': ['autopep8', 'add_blank_lines_for_python_control_statements', 'isort'],
\   'ruby': ['rubocop'],
\   'rust': ['rustfmt']
\}
let g:ale_linters = {
\   'python': ['pylsp'],
\}
let g:ale_fix_on_save = 1
let g:ale_sign_column_always = 1
let g:ale_completion_enabled = 1
let g:ale_set_balloons = 1
let g:ale_javascript_prettier_use_local_config = 1
let g:ale_hover_to_preview = 1
let g:ale_sign_error = ''
let g:ale_sign_warning = ''
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

let g:goyo_linenr = 1
let g:goyo_width = 120
let g:NERDTreeWinSize = max([20, winwidth('%')/6])

let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.jsx,*.tsx,*.mdx,*.html.erb,*.vue'

let g:VimuxOrientation = 'h'
let g:VimuxHeight = '50'

autocmd BufWritePre * :%s/\s\+$//e

let g:vim_vue_plugin_config = {
      \'syntax': {
      \   'template': ['html', 'mustache'],
      \   'script': ['typescript'],
      \   'style': ['css'],
      \},
      \'full_syntax': [],
      \'initial_indent': [],
      \'attribute': 0,
      \'keyword': 0,
      \'foldexpr': 0,
      \'debug': 0,
      \}
